/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miageproject.monhelloworld;

/**
 *
 * @author laura
 */
public class Hello {
    private String chaineInit;

    public String getChaineInit() {
        return chaineInit;
        
    }

    public void setChaineInit(String chaineInit) {
        this.chaineInit = chaineInit;
    }
    
    public Hello(String chaineInit){
        this.chaineInit= chaineInit;   
    }
    
    /**
     * 
     * @param chaine
     * @return retourne la chaine de caractère en entrée préfixée par la chaine initiale de la classe
     */
    public String afficheur(String chaine){
        return chaineInit+""+chaine;
  }

}
